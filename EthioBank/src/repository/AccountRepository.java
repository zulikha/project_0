package com.revature.repository;


import com.revature.model.Account;


public interface AccountRepository {
	Account select(int accountId);
	boolean insert(int accounttypeid);
	int lastAccountId();
	double deposit(int accountid,double amount);
	double withdraw(int accountid,double amount);

}
