package com.revature.repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.revature.model.Employee;
import com.revature.util.ConnectionFactory;

public class EmployeeRepositoryImpl implements EmployeeRepository {

	Connection conn = null;
	Statement stmt = null;
	ResultSet resultset = null;

	public List<Employee> select() {
		List<Employee> employees = new ArrayList<Employee>();
		Employee employee;
		final String SQL = "select * from Employee";
		stmt = null;
		resultset = null;
		
		try {
			Connection conn = ConnectionFactory.getConnectionViaProperties();
			stmt = conn.createStatement();
			stmt.execute(SQL);
			resultset = stmt.getResultSet();
			while (resultset.next()) {
				
				employee = new Employee(resultset.getInt(1), resultset.getString(2), resultset.getString(3),
						 resultset.getString(4), resultset.getString(5), resultset.getString(6)
						);
				employees.add(employee);
			}
//		(int id, String fristName, String lastName, String ssn, String userName, String password) {


		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
				resultset.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return employees;
	}

	

	public boolean insert(Employee employee) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean update(Employee employee) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean delete(Employee employee) {
		// TODO Auto-generated method stub
		return false;
	}

}
