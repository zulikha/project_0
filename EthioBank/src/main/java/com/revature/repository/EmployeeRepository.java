package com.revature.repository;

import java.util.List;

import com.revature.model.Employee;

public interface EmployeeRepository {
	List<Employee> select();

	boolean insert(Employee employee);

	boolean update(Employee employee);

	boolean delete(Employee employee);

}
