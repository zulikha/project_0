package com.revature.repository;

import java.util.List;

import com.revature.model.Application;

public interface AccountApplicationRepository {
	boolean insert(Application application);
	List<Application> select(); 
	boolean update(Application application);
	

}
