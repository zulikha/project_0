package com.revature.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import com.revature.model.Account;
import com.revature.model.Customer;
import com.revature.util.ConnectionClosers;
import com.revature.util.ConnectionFactory;

public class AccountRepositoryImpl implements AccountRepository {
	Statement stmt = null;
	ResultSet resultset=null;
	Connection conn=null;

	//get account info beside on the account id passed
	public Account select(int accountId) {
		PreparedStatement ps=null;
		Account account=new Account();
		final String SQL = "select * from account where id=?";
		
		try {
			conn = ConnectionFactory.getConnectionViaProperties();
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, accountId);
			ps.execute();
			resultset=ps.getResultSet();
			if(resultset.next()) {
				account=new Account(resultset.getInt(1), resultset.getDouble(4), false, resultset.getInt(2), resultset.getString(3));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				resultset.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return account;
	}

	// insert info 
	//this insert it to insert new account with 0 balance with account type passed
	
	public boolean insert(int accounttypeid) {
		List<Customer> customers = new ArrayList<Customer>();
		final String SQL = "insert into account(id, account_type_id , account_number , balance) values(default,?, ?, ?)";
		PreparedStatement stmt = null;
		resultset = null;
		try {
		String accountNumber=UUID.randomUUID().toString().substring(0, 5);

			conn = ConnectionFactory.getConnectionViaProperties();
			stmt = conn.prepareStatement(SQL);
			stmt.setInt(1, accounttypeid);
			stmt.setString(2, accountNumber);
			stmt.setDouble(3, 0.0);
			stmt.execute();
			resultset = stmt.getResultSet();

			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionClosers.closeConnection(conn);
			ConnectionClosers.closeStatement(stmt);
		}
		return false;
	}
	//we need the account saved id
	public int lastAccountId() {
		int idcount=0;
		List<Customer> customers = new ArrayList<Customer>();

		final String SQL = "select count(*) from account as id";
		stmt = null;
		resultset = null;
		
		try {
			Connection conn = ConnectionFactory.getConnectionViaProperties();
			stmt = conn.createStatement();
			stmt.execute(SQL);
			resultset = stmt.getResultSet();
			if(resultset.next()) {
				idcount=resultset.getInt(1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
				resultset.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return idcount;
	}


	public double deposit(int accountid,double amount) {
		PreparedStatement ps=null;
		Account account=new Account();
		account=select(accountid);
		
		if(amount<0) {
			return account.getBalance();
		}
		
		final String SQL = "update account set balance =balance + ? where id=?";
		
		try {
			Connection conn = ConnectionFactory.getConnectionViaProperties();
			ps = conn.prepareStatement(SQL);
			ps.setDouble(1, amount);
			ps.setInt(2, accountid);
			ps.execute();
			
			account=select(accountid);
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				resultset.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return account.getBalance();
	}


	public double withdraw(int accountid,double amount) {
		PreparedStatement ps=null;
		Account account=new Account();
		account=select(accountid);
		
		if(amount>account.getBalance() || amount<0) {
			return account.getBalance();
		}
		
		final String SQL = "update account set balance =balance - ? where id=?";
		
		try {
			Connection conn = ConnectionFactory.getConnectionViaProperties();
			ps = conn.prepareStatement(SQL);
			ps.setDouble(1, amount);
			ps.setInt(2, accountid);
			ps.execute();
			
			account=select(accountid);
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				resultset.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return account.getBalance();
	}


}
