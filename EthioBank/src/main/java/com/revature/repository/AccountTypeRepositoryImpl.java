package com.revature.repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.revature.model.AccountType;

import com.revature.util.ConnectionFactory;

public class AccountTypeRepositoryImpl implements AccountTypeRepository{

	public List<AccountType> select() {
		
		List<AccountType> accountTypes = new ArrayList<AccountType>();
		AccountType accountType;
		final String SQL = "select * from type_of_account";
		Statement stmt = null;
		ResultSet resultset = null;
		
		try {
			Connection conn = ConnectionFactory.getConnectionViaProperties();
			stmt = conn.createStatement();
			stmt.execute(SQL);
			resultset = stmt.getResultSet();
			while (resultset.next()) {

				accountType = new AccountType(resultset.getInt(1), resultset.getString(2), resultset.getDouble(3), resultset.getString(4));
				accountTypes.add(accountType);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
				resultset.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return accountTypes;
	}

	public boolean insert(AccountType accountType) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean update(AccountType accountType) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean delete(AccountType accountType) {
		// TODO Auto-generated method stub
		return false;
	}

}
