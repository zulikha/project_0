package com.revature.repository;

import java.util.List;

import com.revature.model.AccountType;

public interface AccountTypeRepository {
	List<AccountType> select();
	boolean insert(AccountType accountType);
	boolean update(AccountType accountType);
	boolean delete(AccountType accountType);

}
