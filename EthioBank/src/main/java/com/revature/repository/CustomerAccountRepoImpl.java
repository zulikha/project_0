package com.revature.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.revature.util.ConnectionClosers;
import com.revature.util.ConnectionFactory;

public class CustomerAccountRepoImpl implements CustomerAccountRepo {
	Connection conn = null;
	PreparedStatement stmt = null;
	public boolean insert(int customerId, int accountId) {
		final String SQL = "insert into customer_accounts(id, customer_id, account_number) values(default,?,?)";
		stmt = null;

		try {
			conn = ConnectionFactory.getConnectionViaProperties();
			stmt = conn.prepareStatement(SQL);
			stmt.setInt(1, customerId);
			stmt.setInt(2, accountId);
			stmt.execute();

			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionClosers.closeConnection(conn);
			ConnectionClosers.closeStatement(stmt);
		}
		return false;
	}

}
