package com.revature.repository;

import java.util.List;

import com.revature.model.Customer;

public interface CustomerRepository {

	List<Customer> select();
	boolean insert(Customer customer);
	boolean update(Customer customer);
	boolean delete(Customer customer);
	

}