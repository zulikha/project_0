package com.revature.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.revature.model.Customer;
import com.revature.util.ConnectionClosers;
import com.revature.util.ConnectionFactory;

public class CustomerRepositoryImpl implements CustomerRepository {
	private static final Logger LOGGER = LogManager.getLogger(CustomerRepositoryImpl.class);
	Connection conn = null;
	Statement stmt = null;
	ResultSet resultset = null;
// get all customers listed
	public List<Customer> select() {
		List<Customer> customers = new ArrayList<Customer>();

		final String SQL = "select * from customer";
		stmt = null;
		resultset = null;
		
		try {
			Connection conn = ConnectionFactory.getConnectionViaProperties();
			stmt = conn.createStatement();
			Customer custsomer=null;
			stmt.execute(SQL);
			resultset = stmt.getResultSet();
			
			
			while (resultset.next()) {
				custsomer = new Customer(
						resultset.getInt(1),
						resultset.getString(2),
						resultset.getString(3),
						resultset.getInt(4),
						resultset.getString(5),
						resultset.getString(6),
						resultset.getString(7),
						resultset.getString(8));
				customers.add(custsomer);
						}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
				resultset.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		LOGGER.debug("The Accountreposed was successfully called, and it returned: " + customers);
		return customers;
	}
//insert 
	public boolean insert(Customer customer) {
		List<Customer> customers = new ArrayList<Customer>();
		final String SQL = "insert into customer(first_name,last_name,age,ssn,user_name,user_password,email) "
				+ "values (?,?, ?, ?,?,?,?)";

		PreparedStatement stmt = null;
		resultset = null;

	try {

			conn = ConnectionFactory.getConnectionViaProperties();
			stmt = conn.prepareStatement(SQL);
			stmt.setString(1, customer.getFristName());
			stmt.setString(2, customer.getLastName());
			stmt.setInt(3, customer.getAge());
			stmt.setString(4, customer.getSsn());
			stmt.setString(5, customer.getUsername());
			stmt.setString(6, customer.getPassword());
			stmt.setString(7, customer.getEmail());
			
			stmt.execute();
			stmt.getResultSet();

			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionClosers.closeConnection(conn);
			ConnectionClosers.closeStatement(stmt);
		}
		return false;
	}

	public boolean update(Customer customer) {
		Connection conn = null;
		PreparedStatement ps = null;
		final String SQL ="update customer set first_name=?,last_name=? ,age=? ,ssn=?, user_name=? ,user_password=? ,email=?  where id=? ";
		try {
			conn = ConnectionFactory.getConnection();
			ps = conn.prepareStatement(SQL);
			ps.setString(1, customer.getFristName());
			ps.setString(2, customer.getLastName());
			ps.setInt(3, customer.getAge());
			ps.setString(4, customer.getSsn());
			ps.setString(5,null);
			ps.setString(6,customer.getUsername());
			ps.setString(7, customer.getPassword());
			ps.setString(8,customer.getEmail());
			ps.setString(9, null);
			ps.setInt(10, customer.getId());
			ps.execute();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			ConnectionClosers.closeConnection(conn);
			ConnectionClosers.closeStatement(ps);
		}
		
	
		// TODO Auto-generated method stub
		return false;
	}

	public boolean delete(Customer customer) {
		// TODO Auto-generated method stub
		return false;
	}

}
