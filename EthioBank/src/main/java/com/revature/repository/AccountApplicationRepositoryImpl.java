package com.revature.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.revature.model.Application;


import com.revature.util.ConnectionClosers;
import com.revature.util.ConnectionFactory;

public class AccountApplicationRepositoryImpl implements AccountApplicationRepository {
	Connection conn = null;
	PreparedStatement stmt = null;

	//insert the info that been give by the user and insert it db
	public boolean insert(Application application) {
		final String SQL = "insert into application "
				+ "(id,application_date , aprover_id , account_type_id , is_approved ,approved_date , customer_id) "
				+ "values(default,?,?,?,?,?,?)";

		try {
			conn = ConnectionFactory.getConnectionViaProperties();
			stmt = conn.prepareStatement(SQL);
			stmt.setString(1, application.getApplicationDate());
			stmt.setObject(2, null);
			stmt.setInt(3, application.getAccount_type_id());
			stmt.setBoolean(4, false);
			stmt.setString(5, null);
			stmt.setInt(6, application.getCustomer_id());
			stmt.execute();

			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionClosers.closeConnection(conn);
			ConnectionClosers.closeStatement(stmt);
		}
		return false;
	}
// get data from the database
	public List<Application> select() {
		List<Application> applications = new ArrayList<Application>();

		final String SQL = "select * from Application";
		Statement stmt = null;
		ResultSet resultset = null;

		try {
			Connection conn = ConnectionFactory.getConnectionViaProperties();
			stmt = conn.createStatement();
			stmt.execute(SQL);
			resultset = stmt.getResultSet();
			while (resultset.next()) {

				Application application = new Application(resultset.getInt(1), resultset.getString(2),
						resultset.getInt(3), resultset.getInt(4), resultset.getBoolean(5), resultset.getString(6),
						resultset.getInt(7));

				applications.add(application);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
				resultset.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return applications;

	}
//update 
	public boolean update(Application application) {


		final String SQL = "update application set is_approved=?, aprover_id = ? where  id= ?";
	
		try {
			conn = ConnectionFactory.getConnectionViaProperties();
			stmt = conn.prepareStatement(SQL);
			
			stmt.setBoolean(1, application.isAprooved());
			stmt.setInt(2,application.getAproverId());
			stmt.setInt(3, application.getId());

			stmt.execute();

			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionClosers.closeConnection(conn);
			ConnectionClosers.closeStatement(stmt);
		}
		return false;
}
}
