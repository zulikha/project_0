package com.revature.ethiobankapp;

import java.util.Scanner;

import com.revature.controlers.Controller;

public class EthioBankApp {

	public static void main(String[] args) {
		Controller controller = new Controller();
		Scanner scan = new Scanner(System.in);
		

		System.out.println(" Welcome to Ethio Bank \n");
		System.out.println("=============================");
		System.out.println("Do you want to continue?  y/n \n");
		String custemerRespond = scan.nextLine();
		
// asking the user what they want to do
		while (custemerRespond.equalsIgnoreCase("y")) {
			System.out.println("What Do you want to do? Please choose the number listed \n");
			System.out.println("1. Do you want to register a new customer? ");
			System.out.println("2. Do you want see list of customers?" );
			System.out.println("3. Do you want to apply for account?");
			System.out.println("4. Do you want to check application states?");
			System.out.println("5. Do you want to aprove/reject applications?");
			System.out.println("6. Do you want to make a deposit? ");
			System.out.println("7. Do you want to make a withdraw? ");
			System.out.println("8. Do you want to check current balance?");
			System.out.println("9. Do you want to transfer money?");

			int task = scan.nextInt();

			switch (task) {
			case 1:

				controller.custormerRegestration();
				break;
			case 2:
				controller.getAllCustomers();
				
				break;
			case 3:
				controller.accountApplication();

				break;
			case 4:
				controller.applicationStates();
				
				break;
			
			case 5:
				controller.aproveApplication();

				break;
			
			case 6:
				controller.makeDeposit();

				break;
			case 7:

				controller.makeWithdrawl();
				break;
				
			case 8:
				controller.currentBalance();

				break;
				
			case 9:
				controller.transfer();

			default:
				break;
			}

		}
		System.out.println("Do you want to continue?  y/n");
		custemerRespond = scan.nextLine();
	}

}
