package com.revature.controlers;

import java.util.List;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.revature.model.Account;
import com.revature.model.AccountType;
import com.revature.model.Application;
import com.revature.model.Customer;
import com.revature.repository.AccountApplicationRepository;
import com.revature.repository.AccountApplicationRepositoryImpl;
import com.revature.repository.AccountRepository;
import com.revature.repository.AccountRepositoryImpl;
import com.revature.repository.AccountTypeRepository;
import com.revature.repository.AccountTypeRepositoryImpl;
import com.revature.repository.CustomerAccountRepo;
import com.revature.repository.CustomerAccountRepoImpl;
import com.revature.repository.CustomerRepository;
import com.revature.repository.CustomerRepositoryImpl;

public class Controller {
	private static final Logger LOGGER = LogManager.getLogger(Controller.class);
	
	//creating an object for each repositories
	AccountRepository accountRepository = new AccountRepositoryImpl();
	CustomerRepository customerRepository = new CustomerRepositoryImpl();
	AccountTypeRepository accountTypeRepository = new AccountTypeRepositoryImpl();
	AccountApplicationRepository accountApplicationRepository = new AccountApplicationRepositoryImpl();
	CustomerAccountRepo customerAccountRepo = new CustomerAccountRepoImpl();

	Scanner scan = new Scanner(System.in);

	//This method used for registering a customer
	//it recive customer information and create an object
	//it calls customer repository insert method
	public void custormerRegestration() {
		LOGGER.debug("Customer registration start");
		Customer customer = new Customer();
		System.out.println("Customer registration \n");
		System.out.println("Customer first name \n");
		String fName = scan.nextLine();
		if (fName.length() < 2 && fName.length() == 0) {
			System.out.println("you need to add your first name \n");
		}
		System.out.println("Customer last name \n");
		String lName = scan.nextLine();
		if (lName.length() < 1 && lName.length() == 0) {
			System.out.println("you need to add your last name \n");
		}
		System.out.println("Customer social security number name \n");
		String ssn = scan.nextLine();

		System.out.println("Customer age name \n");
		int age = scan.nextInt();
		if (age < 18) {
			System.out.println("you need to be at list 18 in order to apply for account \n");
		}
		System.out.println("Customer username\n");
		String username = scan.next();
		System.out.println("customer password \n");
		String password = scan.next();
		System.out.println("customer Email \n");
		String email = scan.next();
		//set user inputs on the object
		customer.setFristName(fName);
		customer.setLastName(lName);
		customer.setSsn(ssn);
		customer.setAge(age);
		customer.setUsername(username);
		customer.setPassword(password);
		customer.setEmail(email);
		//calling insert method from customerRepository by passing object(customer)
		boolean saved = customerRepository.insert(customer);
		LOGGER.debug("Customer is registerd:" + saved);

		if (saved == true) {
			System.out.println("Customer is registed.");
		} else {
			System.out.println("Customer is not registed. ");
		}

		LOGGER.debug("Customer registration end");
	}

	//this method is used to create account application
	//it shows list of account types
	//it accept the application inforomation
	//it will save the application using accountApplicationRepository insert method
	public void accountApplication() {
		LOGGER.debug("Application start");
		System.out.println("Account application");
		//ask the user for what kinds of account type he/she is applying for
		System.out.println("For what kinds of account type you are applying?");
		List<AccountType> accountTypes = accountTypeRepository.select();
		//show the user the account type options
		for (AccountType x : accountTypes) {
			System.out.println(x.getId() + "." + x.getAccount_type());
		}

		int accountTypeId = scan.nextInt();
		System.out.println("what is your customer id?");

		int customerId = scan.nextInt();

		Application application = new Application();
		application.setCustomer_id(customerId);
		application.setAccount_type_id(accountTypeId);
     // inserting application in into application table
		boolean isSaved = accountApplicationRepository.insert(application);
		System.out.println("Is Application Saved:" + isSaved);
		LOGGER.debug("Application end");

	}

	//this method call customerRepository.select and get list of customer list
	//it display them using for loop
	public List<Customer> getAllCustomers() {
		LOGGER.debug("Get customers list start");
		List<Customer> customers = customerRepository.select();
		System.out.println("Customers List:");
		for (Customer x : customers) {
			System.out.print(x.getId() + " =>");
			System.out.print(" " + x.getFristName() + "\t");
			System.out.print(" " + x.getLastName() + "\t");
			System.out.print(" " + x.getAge() + "\t");
			System.out.print(" " + x.getSsn() + "\t");
			System.out.print(" " + x.getUsername() + "\t");
			System.out.print(" " + x.getPassword() + "\t");
			System.out.print(" " + x.getEmail() + "\t");
			System.out.print(" " + x.getAccounts() + "\n");
		}
		LOGGER.debug("Get customers list end");
		return customers;
	}

	//this method shows existing applications with theier approve state true or false
	public void applicationStates() {
		LOGGER.debug("Show application status list start");
		System.out.println("Application states ");
		List<Application> applications = accountApplicationRepository.select();
		System.out.println("Application List:");

		System.out.println("Id\t Applyed_Date \tEmpId \tapproved \tAprvdDate \tCustid \tacttype");
		for (Application x : applications) {
			System.out.print(" " + x.getId());
			System.out.print(" \t" + x.getApplicationDate());
			System.out.print(" \t" + x.getAproverId());
			System.out.print(" \t" + x.isAprooved());
			System.out.print(" \t \t" + x.getApproved_date());
			System.out.print(" \t" + x.getCustomer_id());
			System.out.print(" \t" + x.getAccount_type_id() + "\n");
		}
		LOGGER.debug("Show application status list end");

	}

	//using this method the employere can approve or disapprove the application
	//it requires employee id, the customer id of the applicant and the application type
	// [employee] approving [custsomer] [application type]
	public void aproveApplication() {
		LOGGER.debug("Show application approval start");
		System.out.println("Approve/Reject");
		applicationStates();

		System.out.println("which application you want to approve/disapprove?");
		int applicationId = scan.nextInt();
		System.out.println("What is the account type id number?");
		int accounttypeid = scan.nextInt();
		System.out.println("What is the customer id number?");
		int customerid = scan.nextInt();

		System.out.println("what is your empplyee id?");
		int approverId = scan.nextInt();
		System.out.println("do you want to approve it? y/n");
		String approverAnswer = scan.next();
		Application application = new Application();

		application.setAproverId(approverId);
		application.setId(applicationId);
		application.setAccount_type_id(accounttypeid);
		application.setCustomer_id(customerid);
		if (approverAnswer.equalsIgnoreCase("y")) {

			application.setAprooved(true);
		} else {
			application.setAprooved(false);

		}
		boolean isupdated = accountApplicationRepository.update(application);

		System.out.println("Is updated:" + isupdated);

		if (isupdated == true) {
			boolean isaccuntcreated = accountRepository.insert(accounttypeid);
			if (isaccuntcreated == true) {
				int accountid = accountRepository.lastAccountId();
				boolean iscustomeraccountsaved = customerAccountRepo.insert(customerid, accountid);
				System.out.println(
						"Customer id:" + customerid + " has accountId:" + accountid + " >>" + iscustomeraccountsaved);

			}

		}
		LOGGER.debug("Show application approval end");

	}

	//this method used for making a deposit
	//we call accountRepository.deposit
	//we need to pass account id where we make deposit and the amount
	public double makeDeposit() {
		LOGGER.debug("Show make deposit start");
		System.out.println("Deposit");
		// Scanner scan = new Scanner(System.in);
		System.out.println("What is the account id?");
		int id = scan.nextInt();
		System.out.println("How much you want to deposit");
		double amount = scan.nextDouble();
		double currentbalanc = accountRepository.deposit(id, amount);
		System.out.println("your current balance is:" + currentbalanc);
		LOGGER.debug("Show make deposit end");
		return currentbalanc;
		

	}

	//this method used for making a withdrawal
		//we call accountRepository.withdrawal
		//we need to pass account id where we make withdrawal and the amount
	public double makeWithdrawl() {
		LOGGER.debug("Show make withdrawal start");
		System.out.println("Withdral");
		System.out.println("What is the account id?");
		int id = scan.nextInt();
		System.out.println("How much you want to withdraw");
		double amount = scan.nextDouble();
		double currentbalanc = accountRepository.withdraw(id, amount);
		System.out.println("your current balance is:" + currentbalanc);
		LOGGER.debug("Show make withdrawal end");
		return currentbalanc;
	}

	
	//this method used for showing current balance of the customer
	//we accept the customer id to show the balance
	public double currentBalance() {
		LOGGER.debug("Show current balance start");
		System.out.println("Current balance");
		System.out.println("What is the account Id?");
		int id = scan.nextInt();
		Account account = accountRepository.select(id);
		System.out.println("your Current balance is: " + account.getBalance());
		LOGGER.debug("Show current balance end");
		return account.getBalance();

	}
	//this method used for transfering balance from one account to the other
	//we accept the account number from and to account
	//we check if there is enouugh balance
	//we transfer the amount if there is enough balance
	public void transfer() {
		LOGGER.debug("making transfer start");
		System.out.println("What is the account id?");
		int idfrom = scan.nextInt();
		System.out.println("what is the account number you want to transfer");
		int idto = scan.nextInt();
		System.out.println("how much you want to transfer?");
		double transferamount = scan.nextDouble();

		Account accountfrom = accountRepository.select(idfrom);

		if (transferamount > accountfrom.getBalance()) {
			System.out.println(
					"Current balance is: " + accountfrom.getBalance() + "you don't have enough money to transfer");
		} else {
			double currentbalancfrom = accountRepository.withdraw(idfrom, transferamount);
			System.out.println("your current balance on act: " + idfrom + " wiil be " + currentbalancfrom);
			double currentbalancto = accountRepository.deposit(idto, transferamount);
			System.out.println("your balance on the other account is " + currentbalancto);

		}
		scan.close();
		LOGGER.debug("your money was transfer correctlly");
		LOGGER.debug("making transfer end");

	}

}
