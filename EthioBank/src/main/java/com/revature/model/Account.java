package com.revature.model;


public class Account {
	private int id;
	private double balance;
	private boolean isJoint;
	private int accountTypeId;
	private String accountNumber;
	
	public Account() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Account(int id, double balance, boolean isJoint, int accountTypeId, String accountNumber) {
		super();
		this.id = id;
		this.balance = balance;
		this.isJoint = isJoint;
		this.accountTypeId = accountTypeId;
		this.accountNumber = accountNumber;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public boolean isJoint() {
		return isJoint;
	}
	public void setJoint(boolean isJoint) {
		this.isJoint = isJoint;
	}
	public int getAccountTypeId() {
		return accountTypeId;
	}
	public void setAccountTypeId(int accountTypeId) {
		this.accountTypeId = accountTypeId;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
}
