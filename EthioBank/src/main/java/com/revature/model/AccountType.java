package com.revature.model;

public class AccountType {
	private int id;
	private String account_type;
	private double interest_rate;
	private String description;
	private int customer_id;
	
	
	
	public int getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}
	public AccountType() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AccountType(int id, String account_type, double interest_rate, String description) {
		super();
		this.id = id;
		this.account_type = account_type;
		this.interest_rate = interest_rate;
		this.description = description;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAccount_type() {
		return account_type;
	}
	public void setAccount_type(String account_type) {
		this.account_type = account_type;
	}
	public double getInterest_rate() {
		return interest_rate;
	}
	public void setInterest_rate(double interest_rate) {
		this.interest_rate = interest_rate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
	
	

}
