package com.revature.model;


public class Employee {
	private int id;
	private String fristName;
	private String lastName;
	private String ssn;
	private String userName;
	private String password;

	public Employee() {
		super();
	}

	public Employee(int id, String fristName, String lastName, String ssn, String userName, String password) {
		super();
		this.id = id;
		this.fristName = fristName;
		this.lastName = lastName;
		this.ssn = ssn;
		this.userName = userName;
		this.password = password;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFristName() {
		return fristName;
	}

	public void setFristName(String fristName) {
		this.fristName = fristName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	

}
