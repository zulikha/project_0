package com.revature.model;

public class Application {
	private int id;
	private String applicationDate;
	private int aproverId;
	private int account_type_id;
	private boolean isAprooved;
	private String approved_date;
	private int customer_id;

	public Application() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Application(int id, String applicationDate, int aproverId, int account_type_id, boolean isAprooved,
			String approved_date, int customer_id) {
		super();
		this.id = id;
		this.applicationDate = applicationDate;
		this.aproverId = aproverId;
		this.account_type_id = account_type_id;
		this.isAprooved = isAprooved;
		this.setApproved_date(approved_date);
		this.customer_id = customer_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}

	public int getAccount_type_id() {
		return account_type_id;
	}

	public void setAccount_type_id(int account_type_id) {
		this.account_type_id = account_type_id;
	}

	public boolean isAprooved() {
		return isAprooved;
	}

	public void setAprooved(boolean isAprooved) {
		this.isAprooved = isAprooved;
	}

	public int getAproverId() {
		return aproverId;
	}

	public void setAproverId(int aproverId) {
		this.aproverId = aproverId;
	}

	public String getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(String applicationDate) {
		this.applicationDate = applicationDate;
	}

	public String getApproved_date() {
		return approved_date;
	}

	public void setApproved_date(String approved_date) {
		this.approved_date = approved_date;
	}

}
