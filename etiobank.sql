drop table if exists application ;
drop table if exists customer_accounts ; 
drop table if exists customer ;
drop table if exists account; 
drop table if exists type_of_account;
drop table if exists employee; 
--employee

create table employee(
						id  SERIAL PRIMARY KEY,
						first_name varchar,
						last_name varchar,
						ssn varchar);
insert into employee (ID, first_name,last_name, ssn)  values(default,'kebede','Alemu','000-00-0000');
insert into employee (ID, first_name,last_name, ssn) values(default,'Tesema','lema','000-00-0000');
insert into employee (ID, first_name,last_name, ssn) values(default,'Hayile','biru','000-00-0000');

select * from employee;
--typeofaccount

create table type_of_account(
						id serial primary key,
						account_type varchar,
						interest_rate numeric,
						description varchar);
					
insert into type_of_account (id,account_type, interest_rate,description) values(default,'Checking', 1.2, 'desc');
insert into type_of_account (id,account_type, interest_rate,description) values(default,'Saving', 1.3, 'desc1');
insert into type_of_account (id,account_type, interest_rate,description) values(default,'Investment', 1.4, 'desc1');
select * from type_of_account;
--account

create table account(
					id serial primary key,
					account_type_id int8 references type_of_account(id),
					account_number varchar, 
					balance numeric);

insert into account values(1, 2, '0000001', 100);
insert into account values(2, 2, '0000002', 200);
insert into account values(3, 1, '0000003', 300);
insert into account(id, account_type_id , account_number , balance) values(default, 1, '0000001xss', 100);
--insert into account (id, account_type_id , account_number , balance ) values(default, 1, '252ab', 0.0)
--select count(*) from account as id;
--select * from account where account_number='0000001' ;
--select * from account where id=1;
--update account set balance =balance -1 where id=1;
--insert into account (id, account_type_id , account_number , balance ) values(default, 3, '14cfd', 0.0);
select * from account;

--customer

create table customer (
						id  SERIAL PRIMARY key,
						first_name varchar,
						last_name varchar,
						age numeric,
						ssn varchar ,
						account_type varchar, 
						user_name varchar,
						user_password varchar,
						email varchar);
					
					
insert into customer values(1, 'Zulikha', 'Shafi', 19, '333769890','Checking','zxxxx','xxxxxx','xxxxx@xxx');
insert into customer values(2, 'Ezedin', 'Wangoria', 34, '333769834','Saving''zxxxx','xxxxxx','xxxxx@xxx');
insert into customer values(3, 'Zikra', 'Asefa', 56, '333769890','both','zxxxx','xxxxxx','xxxxx@xxx');
insert into customer(first_name,last_name,age,ssn,user_name,user_password,email) values ('ee','ee', 44, 'ee','ee','ee','ee');
--select * from customer;
select * from customer;
--accountcustomer

create table customer_accounts(
id SERIAL,
customer_id int8 references customer(id),
account_number int8 references account(id)
);

insert into customer_accounts values(1,2,1);
insert into customer_accounts values(2,2,2);
insert into customer_accounts values(3,1,3);
insert into customer_accounts(id, customer_id, account_number) values(default,1,1);


select * from  customer_accounts;
--application

create table application(
						 id serial primary key,
						 application_date varchar, 
						 aprover_id int8 references employee(id) ,
						 account_type_id int8 references type_of_account(id),
						 is_approved boolean,
					     approved_date varchar,
					     customer_id int8 references customer(id)
					     );
insert into application(application_date , aprover_id , account_type_id , is_approved ,approved_date , customer_id) values(	'12-23-2020',null,1,true,'12-23-2020',1);
insert into application values(2,'12-23-2020',1,1,true,'12-24-2020',2);
insert into application values(3,'12-23-2020',1,1,true,'12-25-2020',3);
update application set is_approved=false, aprover_id = 2 where  id= 1;
select * from application;
						